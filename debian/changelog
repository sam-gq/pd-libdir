pd-libdir (1.11-5) UNRELEASED; urgency=medium

  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 24 Feb 2020 10:37:21 +0000

pd-libdir (1.11-4) unstable; urgency=medium

  * Switched to system-provided Makefile.pdlibbuilder
    * Pass build-flags to all 'make' invocations

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 05 Feb 2018 14:40:17 +0100

pd-libdir (1.11-3) unstable; urgency=medium

  * Simplified & unified d/rules
    * Enabled hardening
  * Updated Vcs-* stanzas to salsa.d.o
  * Updated maintainer address
  * Bumped dh compat to 11
  * Switched URLs to https://
  * Updated d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 01 Feb 2018 23:18:32 +0100

pd-libdir (1.11-2) unstable; urgency=medium

  * Fix FTCBFS: Use the host architecture strip.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: #883280)
  * Use https:// for homepage field
  * Fixed spelling error in d/changelog
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 15 Jan 2018 13:40:52 +0100

pd-libdir (1.11-1) unstable; urgency=medium

  * New upstream version 1.11
    (Closes: #872681)
  * Demoted pd-import to "Suggests"
  * Bumped standards version to 4.1.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 22 Aug 2017 18:02:20 +0200

pd-libdir (1.10-1) unstable; urgency=medium

  * New upstream version 1.10

  [ Hans-Christoph Steiner ]
  * Depends: puredata-core | pd, so the depends is not only on a virtual package
  * Updated to copyright-format/1.0
  * Removed 'DM-Upload-Allowed: yes', its deprecated

  [ IOhannes m zmölnig ]
  * Dropped patches
  * Updated d/rules
  * Updated d/control
    * Dropped dependency on quilt
    * Canonical Vcs-* stanzas (using https://)
    * Added myself to uploaders
  * Fixed d/copyright
  * Updated d/watch to github
  * Bumped dh-compat to 9
  * Bumped standards version to 3.9.8

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sat, 14 Jan 2017 22:33:52 +0100

pd-libdir (1.9-3) unstable; urgency=low

  * bumped standards version to 3.9.2
  * updated Build-Depends to use puredata-dev (Closes: #629791)

 -- Hans-Christoph Steiner <hans@eds.org>  Sat, 11 Jun 2011 18:50:59 -0400

pd-libdir (1.9-2) unstable; urgency=low

  * patched Makefile to build on kFreeBSD and Hurd (Closes: #605828)

 -- Hans-Christoph Steiner <hans@eds.org>  Sun, 19 Dec 2010 14:39:57 -0500

pd-libdir (1.9-1) unstable; urgency=low

  [ Hans-Christoph Steiner ]
  * Initial release (Closes: #595972)

  [ Felipe Sateler ]
  * Include the whole BSD text in the copyright file

 -- Felipe Sateler <fsateler@debian.org>  Thu, 28 Oct 2010 19:26:18 -0300
